# FS Simulator
Official documentation can be found [here](https://fs-driverless.github.io/Formula-Student-Driverless-Simulator/latest/). You can find working example with data capture from Cameras and Lidar in `\python\selfdriving.py`.

## Installation
1. Download [FS Simulator](https://github.com/FS-Driverless/Formula-Student-Driverless-Simulator/releases/download/v2.0.0/fsds-v2.0.0-win.zip) and extract it into `C:\Users\username\Formula-Student-Driverless-Simulator`
2. Pull this repo into `Formula-Student-Driverless-Simulator` folder
3. Go into `Formula-Student-Driverless-Simulator\python` and run `pip install -r requirements.txt`
4. Run `python selfdriving.py`