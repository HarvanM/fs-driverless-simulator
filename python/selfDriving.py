import sys
import os
import airsim
import numpy as np
import cv2
import fsds

# Get camera images from simulator
def getCameraImages(cameras):
    imageRequests = []
    for camera in cameras:
        # create request for all cameras
        imageRequests.append(airsim.ImageRequest(camera_name = camera, image_type = fsds.ImageType.Scene, pixels_as_float = False, compress = False))
    images = client.simGetImages(imageRequests)
    imagesRgb = []
    # assemble images into compatible numpy array
    for image in images:
        imageArr = np.frombuffer(image.image_data_uint8, dtype=np.uint8)
        imagesRgb.append(imageArr.reshape(image.height, image.width, 3))
    return imagesRgb

def showImages(images):
    finalImage = np.concatenate((images[0], images[1]), axis= 0)
    cv2.imshow('Cameras', finalImage)

def parse_lidarData(point_cloud):
    """
    Takes an array of float points and converts it into an array with 3-item arrays representing x, y and z
    """
    points = np.array(point_cloud, dtype=np.dtype('f4'))
    return np.reshape(points, (int(points.shape[0]/3), 3))

## adds the fsds package located the parent directory to the pyhthon path
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

# connect to the simulator 
client = fsds.FSDSClient()

# Check network connection, exit if not connected
client.confirmConnection()

while True:
    # Cameras
    images = getCameraImages(["cam1", "cam2"])
    showImages(images)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    # Lidar
    lidardata = client.getLidarData(lidar_name = 'Lidar', vehicle_name = 'FSCar')
    points = parse_lidarData(lidardata.point_cloud)
    #print("point 0  X: %f  Y: %f  Z: %f" % (points[0][0], points[0][1], points[0][2]))

cv2.destroyAllWindows()
